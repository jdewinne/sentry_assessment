# Sentry.io - SE Technical Exercise

## Instructions

Please reply to answers in a markdown file (e.g., `my-answers.md`). Feel free to include any links and other resources (images, GIFs, etc.).

Once done, zip everything up (markdown file and any relevant resources/attachments) and send via email to recruiter and hiring manager.

Email *neil@sentry.io* if you have any questions.


##  1) JavaScript: Ignore errors from 3rd party library?
> Hi,
> I want to ignore all errors that come from `https://somejslibrary.com/somelibrary.js` as they are not impacting my application/experience and I can’t do anything about these errors since I don’t control the code/library. Here is my code/setup:


    <!DOCTYPE html>
    <html>
      <head>
        <title>Sample Title</title>
        <script src="https://browser.sentry-cdn.com/4.0.5/bundle.min.js" crossorigin="anonymous"></script>
        <script>
          Sentry.init({
            dsn: 'https://XXXXXXX@sentry.io/123',
            release: "1.1"
          });
        </script>
    
        <script src="https://somejslibrary.com/somelibrary.js" crossorigin="anonymous"></script>
    
        <script>
          var obj = {};
          obj.invalidFunction(); // this error should always be reported to Sentry
        </script>
      </head>
      <body>
          Body text...
      </body>
    </html>

*Hint:* Check out https://docs.sentry.io/learn/configuration/?platform=browser. `somelibrary.js` does not exist, feel free to insert valid library/script to test/verify, and replace with `somelibrary.js` before submitting.

Please show working code/snippet with explanation + appropriate references (e.g., relevant link(s)) and write in the form of an email response to a customer explaining how to ignore errors from 3rd party library.


## 2) A mid-size company that is security conscious

(Assume Sentry is both a single-tenant and multi-tenant solution)

> Hi,
> I am a developer at company XYZ and we are extremely security conscious as we have been operating in "stealth"-mode due to the proprietary nature of our product and space as well as we have not released our product and do not want competitors/public to know about anything unless an announcement has approved by the C-level staff.
> I noticed that Sentry has a hosted single-tenant and multi-tenant solution, which one would you recommend and why?
> I'm looking to present this to my boss and team next week at our monthly tech summit.

*Hint(s)*: https://sentry.io/security. Don't bother searching for "multi-tenant" on sentry web pages as it does not exist.


## 3) Alert when errors spike?
> Hi,
> I want only to be alerted via email if a specific error occurs more than ten times in an hour. Is that possible? If so, could you please explain how to get this alerting behavior configured?

*Hint:* Create a Sentry account, org, and project. Then go to "Settings" from the project page.


## 4) Angular Project with Sentry integration
> Hi,
> My company is looking to move from our proprietary in-house JS framework to Angular. Before we do that, we wanted to settle on an error reporting service and test it out with Angular so that we can have this configured before the migration. We want to know about errors as we are porting/migrating code so that we are not transferring technical debt in the form of errors and exceptions from our old codebase to the new Angular stack)
> Could you possibly send me a sample angular project (just an app with a button that causes an error will suffice) with Sentry integrated? An example will help me/team integrate correctly.
> An app where clicking the button generates an error which is sent to Sentry would be fantastic.

*Hint(s):* Spin up an angular project and head to https://docs.sentry.io/platforms/javascript/angular/ for Sentry integration. Do not use the old/legacy/deprecated JS SDK (referred to as Raven).

Please link to public GitHub repository which has working code.
The code should be in a state where anyone can clone it and run it.


## 4.1) Bonus: Source maps
> Hi,
> I would also be interested in figuring out how I can get my source maps up to Sentry so that we can see original stack trace rather than the minified JS stacktrace.
> What would this look like and when/where should I upload the source maps?

Hint: Check out https://docs.sentry.io/platforms/javascript/sourcemaps/

