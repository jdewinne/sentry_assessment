rm -rf pdf
mkdir -p pdf
docker run --rm --name pandoc -v `pwd`:/source jagregory/pandoc "1.JavaScript: Ignore errors from 3rd party library.md" -o pdf/q1.pdf
docker run --rm --name pandoc -v `pwd`:/source jagregory/pandoc "2.A mid-size company that is security conscious.md" -o pdf/q2.pdf
docker run --rm --name pandoc -v `pwd`:/source jagregory/pandoc "3.Alert when errors spike.md" -o pdf/q3.pdf
docker run --rm --name pandoc -v `pwd`:/source jagregory/pandoc "4.Angular Project with Sentry integration.md" -o pdf/q4.pdf
docker run --rm --name pandoc -v `pwd`:/source jagregory/pandoc "4.1.Bonus: Source maps.md" -o pdf/q4.1.pdf